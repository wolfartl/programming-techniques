#include <cassert>
#include "integrate.hpp"

integrate(const std::function<double(double)>& f, const double x0, const double xf, const int n_intervals) {

  assert(xf>x0)l
  
  double binSize, x[n_intervals], result = 0;


  binSize = (xf-x0)/n_intervals;

  x[0] = x0;

  for(int i = 0; i<=n_intervals; ++i){

    x[i] = x0 + binSize*i;

  }

  for(int i = 0; i<=n_intervals/2; ++i){
    result += f(x[2*i - 2]) + 4*f(x[2*i-1]) + f(x[2*i]);
  }
  return result*binSize/3;
}

 
