#include <iostream>
#include <cmath>
#include <functional>
#include "integrate.hpp"



int main(){
    // Improve the test.
    auto f = [](double x){return std::sin(x); };

    // You can also declare the function as
    // auto f = (double(*)(double))& std::sin;

    const double res = integrate(f, 0,  M_PI / 3, 10);

    const auto diff = std::abs(res - std::cos(M_PI/ 3));

    std::cout << (diff < 1e-5 ? "SUCCESS" : "FAILURE") << std::endl;
}
