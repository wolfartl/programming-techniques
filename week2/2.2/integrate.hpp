#ifndef INTEGRATE_H
#define INTEGRATE_H
//PRE: x0<xf
//POST: return the result of the definite simpson integral from the point x0 to the point xf 
double integrate(const std::function<double(double)>& f, const double x0, const double xf, const int n_intervals);
#endif /*INTEGRATE_H*/
