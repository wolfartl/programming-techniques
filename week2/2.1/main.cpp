#include <iostream>
#include <cmath>

void read(double *a, const unsigned size){
  
  for (int i = 0; i < size; ++i){
    std::cin >> a[i];
  }
}

void normalize(double *a, const unsigned size){

  double sum_of_squares;

  for(int i = 0; i < size; ++i){
    sum_of_squares += (*a)*(*;
  }
  
  double norm = std::sqrt(sum_of_squares);

  for(int i = 0; i < size; ++i){
    a[i] /= norm;
  }
}

void print_reverse(double a[], const unsigned size){
  for( int i = size; i > 0; --i)
    std::cout << a[i];
}

int main(){
  int size;
  std::cout<<"What is the size of the array?"<<std::endl;
  std::cin>>size;
  
  double a[size];
  
  read(a, size);
  normalize(a, size);
  print_reverse(a, size);

  return 0;  
}

  
